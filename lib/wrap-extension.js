'use strict';

const Halley = require('halley/backbone');

const { Promise } = Halley;
const log = require('loglevel');

function wrapExtension(fn) {
  return function(message, callback) {
    const self = this;
    return Promise.try(
      () =>
        new Promise(resolve => {
          fn.call(self, message, resolve);
        }),
    )
      .catch(err => {
        log.error('Extension failed: ', err.stack || err);
        return message;
      })
      .then(message => {
        callback(message);
      });
  };
}

module.exports = wrapExtension;
