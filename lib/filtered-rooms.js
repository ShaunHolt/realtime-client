'use strict';

const sortsFilters = require('./sorts-filters');
const SimpleFilteredCollection = require('./simple-filtered-collection');

function apply(collection, filter, sort) {
  return new SimpleFilteredCollection([], {
    model: collection.model,
    collection,
    comparator: sort,
    filter,
    autoResort: true,
  });
}

module.exports = {
  favourites(collection) {
    return apply(
      collection,
      sortsFilters.model.favourites.filter,
      sortsFilters.model.favourites.sort,
    );
  },
  recents(collection) {
    return apply(collection, sortsFilters.model.recents.filter, sortsFilters.model.recents.sort);
  },
  unreads(collection) {
    return apply(collection, sortsFilters.model.unreads.filter, sortsFilters.model.unreads.sort);
  },
};
