'use strict';

const realtimeClient = require('..');
require('loglevel').setLevel('debug');

const blocked = require('blocked');

blocked(ms => {
  if (ms > 0) {
    // console.log('BLOCKED FOR %sms', ms);
  }
});

const client = new realtimeClient.RealtimeClient({
  // fayeUrl: 'https://ws-beta.gitter.im/faye',
  token: process.env.GITTER_TOKEN,
  fayeOptions: {},
});

const rooms = new realtimeClient.RoomCollection([], { client, listen: true });

const favs = realtimeClient.filteredRooms.favourites(rooms);

/* Display all the favs, all the time */
favs.on('add remove reset change', () => {
  // console.log(favs.toJSON());
  // console.log('change');
});

rooms.on('change:unreadItems', (/* model */) => {
  // console.log(model.get('uri') + ' ' + model.get('unreadItems'))
  // console.log('unread');
});
